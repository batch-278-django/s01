from django.shortcuts import render, redirect

# Create your views here.

from django.http import HttpResponse
from .models import GroceryItem
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict


# def index(request):
#     groceryitem_list = GroceryItem.objects.all()
#     template = loader.get_template("index.html")
#     context = {
#         'groceryitem_list': groceryitem_list
#     }
#     return HttpResponse(template.render(context, request))


def index(request):
    groceryitem_list = GroceryItem.objects.all()
    # template = loader.get_template("index.html")
    context = {
        'groceryitem_list': groceryitem_list,
        'user': request.user,
    }
    # output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
    # render(request, route_template, context)
    return render(request, "index.html", context)


def groceryitem(request, groceryitem_id):
    # response = f"You are viewing the details of {groceryitem_id}"
    # return HttpResponse(response)

    groceryitem = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
    return render(request, "groceryitem.html", groceryitem)


def register(request):
    users = User.objects.all()
    is_user_registered = False

    user = User()
    user.username = "zoro"
    user.first_name = "Zoro"
    user.last_name = "Roronoa"
    user.email = "zoro@mail.com"
    user.set_password("zoro1234")
    user.is_staff = False
    user.is_active = True

    # check if the user is already registered
    for individual_user in users:
        if individual_user.username == user.username:
            is_user_registered = True
            break
    if not is_user_registered:
        user.save()

    context = {
        'is_user_registered': is_user_registered,
        'first_name': user.first_name,
        'last_name': user.last_name
    }

    return render(request, 'register.html', context)


def change_password(request):
    is_user_authenticated = False

    user = authenticate(request, username='zoro', password='zoro1234')
    if user is not None:
        authenticated_user = User.objects.get(username='zoro')
        authenticated_user.set_password('zoro12345')
        authenticated_user.save()
        is_user_authenticated = True

    context = {
        'is_user_authenticated': is_user_authenticated
    }

    return render(request, 'change_password.html', context)


def login_user(request):
    username = 'luffy'
    password = 'luffy1234'

    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('index')
    else:
        return render(request, 'login.html', {'error_message': 'Invalid login'})


def logout_user(request):
    logout(request)
    return redirect('index')

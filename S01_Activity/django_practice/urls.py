from django.urls import path

# import views from the current directory
from . import views



# urlpatterns is a list of paths that Django will try to match the requested URL to find the correct view.
urlpatterns = [
    path('index', views.index, name='index'),
    path('<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem'),
    path('register', views.register, name='register'),
    path('change_password', views.change_password, name='change_password'),
    path('signin', views.login_user, name='login'),
    path('logout', views.logout_user, name='logout')
]



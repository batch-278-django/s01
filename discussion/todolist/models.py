from django.db import models

# Create your models here.
from django.contrib.auth.models import User


class ToDoItem(models.Model):
    task_name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="Pending")
    date_created = models.DateTimeField('date created', auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.RESTRICT, default="")


class Events(models.Model):
    event_name = models.CharField(max_length=20)
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="Pending")
    date_created = models.DateTimeField('date created', auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.RESTRICT, default="")

from django.shortcuts import render, redirect, get_object_or_404

from django.contrib import messages

# Create your views here.

# The "from" keyword is used to import a module or a function from a module.
from django.http import HttpResponse

import time

# Local imports
from .models import ToDoItem, Events

# import the built-in user model
from django.contrib.auth.models import User

# import the authentication module
from django.contrib.auth import authenticate, login, logout

# to use the template created
# from django.template import loader

# to use the model_to_dict function
from django.forms.models import model_to_dict

# import the LoginForm class
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, AddEventForm, UpdateEventForm

from django.utils import timezone


def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
    events_list = Events.objects.filter(user_id=request.user.id)
    # template = loader.get_template("index.html")
    context = {
        'todoitem_list': todoitem_list,
        'events_list': events_list,
        'user': request.user,
    }
    # output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
    # render(request, route_template, context)
    return render(request, "index.html", context)


def todoitem(request, todoitem_id):
    # response = f"You are viewing the details of {todoitem_id}"
    # return HttpResponse(response)

    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)

    return render(request, "todoitem.html", model_to_dict(todoitem))


# this function is responsible for registering on our application
def register(request):
    context = {}

    if request.method == "POST":
        form = RegisterForm(request.POST)

        if not form.is_valid():
            form = RegisterForm()

        else:
            # get the form data
            user = User()
            user.username = form.cleaned_data['username']
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.email = form.cleaned_data['email']
            password = (form.cleaned_data['password1'])
            user.is_staff = False
            user.is_active = True

            # check if the password and confirm password match
            if password == form.cleaned_data['password2']:
                # check if the user is already registered
                if User.objects.filter(username=user.username).exists():
                    context['error1'] = "Username already exists"
                elif User.objects.filter(email=user.email).exists():
                    context['error2'] = "Email already exists"
                else:
                    # to save our user
                    user.set_password(password)
                    user.save()
                    context['success'] = True
            else:
                context['error3'] = "Passwords do not match"

    return render(request, 'register.html', context)

# old register function
# def register(request):
#     users = User.objects.all()
#     is_user_registered = False
#
#     user = User()
#     user.username = "janedoe"
#     user.first_name = "Jane"
#     user.last_name = "Doe"
#     user.email = "jane@mail.com"
#     user.set_password("jane1234")
#     user.is_staff = False
#     user.is_active = True
#
#     # check if the user is already registered
#     for indiv_user in users:
#         if indiv_user.username == user.username:
#             is_user_registered = True
#             break
#     if not is_user_registered:
#         # to save our user
#         user.save()
#
#     context = {
#         'is_user_registered': is_user_registered,
#         'first_name': user.first_name,
#         'last_name': user.last_name
#     }
#
#     return render(request, 'register.html', context)


def change_password(request):
    is_user_authenticated = False

    user = authenticate(request, username='johndoe', password='john12345')
    if user is not None:
        authenticated_user = User.objects.get(username='johndoe')
        authenticated_user.set_password('john123456')
        authenticated_user.save()
        is_user_authenticated = True

    context = {
        'is_user_authenticated': is_user_authenticated
    }

    return render(request, 'change_password.html', context)


def login_user(request):
    context = {}

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()

        else:
            print(form)
            # process the data in form.cleaned_data as required
            # it retrieves the username and password from the form
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)

            if user is not None:
                context = {
                    'username': username,
                    'password': password
                }

                login(request, user)
                return redirect("todolist:index")
            else:
                context = {
                    'error': True
                }

    return render(request, 'login.html', context)


def logout_user(request):
    logout(request)
    return redirect("todolist:index")


def add_task(request):
    context = {}

    if request.method == 'POST':
        form = AddTaskForm(request.POST)

        if not form.is_valid():
            form = AddTaskForm()

        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

            duplicates = ToDoItem.objects.filter(task_name=task_name, user_id=request.user.id)

            if not duplicates:
                # create a new task and save it to the database
                ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(),
                                        user_id=request.user.id)

                return redirect("todolist:index")

            else:
                context = {
                    'error': True
                }

    return render(request, 'add_task.html', context)


def update_task(request, todoitem_id):

    todoitem = ToDoItem.objects.filter(pk=todoitem_id)

    print(todoitem)

    context = {
        'user': request.user,
        'todoitem_id': todoitem_id,
        'task_name': todoitem[0].task_name,
        'description': todoitem[0].description,
        'status': todoitem[0].status
    }

    if request.method == 'POST':
        form = UpdateTaskForm(request.POST)

        if not form.is_valid():
            form = UpdateTaskForm()

        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:
                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status

                todoitem[0].save()

                return redirect("todolist:viewtodoitem", todoitem_id=todoitem_id)

            else:
                context = {
                    'error': True
                }

    return render(request, 'update_task.html', context)


def delete_task(request, todoitem_id):

    todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()

    return redirect("todolist:index")


def add_event(request):
    context = {}

    if request.method == 'POST':
        form = AddEventForm(request.POST)

        if not form.is_valid():
            form = AddEventForm()

        else:
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']

            duplicates = Events.objects.filter(event_name=event_name, user_id=request.user.id)

            if not duplicates:
                # create a new event and save it to the database
                Events.objects.create(event_name=event_name, description=description, date_created=timezone.now(),
                                      user_id=request.user.id)

                return redirect("todolist:index")

            else:
                context = {
                    'error': True
                }

    return render(request, 'add_event.html', context)


def view_event(request, event_id):

    event = Events.objects.filter(pk=event_id)

    context = {
        'user': request.user,
        'event_id': event_id,
        'event_name': event[0].event_name,
        'description': event[0].description,
        'date_created': event[0].date_created,
        'status': event[0].status
    }

    return render(request, 'view_event.html', context)


def update_event(request, event_id):

    event = Events.objects.filter(pk=event_id)

    context = {
        'user': request.user,
        'event_id': event_id,
        'event_name': event[0].event_name,
        'description': event[0].description,
        'status': event[0].status
    }

    if request.method == 'POST':
        form = UpdateEventForm(request.POST)

        if not form.is_valid():
            form = UpdateEventForm()

        else:
            event_name = form.cleaned_data['event_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if event:
                event[0].event_name = event_name
                event[0].description = description
                event[0].status = status

                event[0].save()

                return redirect("todolist:viewevent", event_id=event_id)

            else:
                context = {
                    'error': True
                }

    return render(request, 'update_event.html', context)


def delete_event(request, event_id):
    print(event_id)
    event = Events.objects.filter(pk=event_id).delete()

    return redirect("todolist:index")







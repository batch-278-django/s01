from django.urls import path

# import views from the current directory
from . import views

app_name = 'todolist'

# urlpatterns is a list of paths that Django will try to match the requested URL to find the correct view.
urlpatterns = [
    # path() function is passed four arguments, two required: route and view, and two optional: kwargs, and name.
    # route is a string that contains a URL pattern.
    # view is a function that is called when Django matches the requested URL to this route.
    # kwargs is a dictionary of additional arguments that can be passed to the view function.
    # name is the name of the URL that will be used to identify the view.
    path('index', views.index, name='index'),
    path('<int:todoitem_id>/', views.todoitem, name='viewtodoitem'),
    path('register', views.register, name='register'),
    path('change_password', views.change_password, name='change_password'),
    path('login', views.login_user, name='login'),
    path('logout', views.logout_user, name='logout'),
    path('add_task', views.add_task, name='add_task'),
    path('<int:todoitem_id>/update_task', views.update_task, name='update_task'),
    path('<int:todoitem_id>/delete_task', views.delete_task, name='delete_task'),
    path('add_event', views.add_event, name='add_event'),
    path('events/<int:event_id>/', views.view_event, name='viewevent'),
    path('events/<int:event_id>/update_event', views.update_event, name='update_event'),
    path('events/<int:event_id>/delete_event', views.delete_event, name='delete_event'),

]



# [SECTION] Models
# Each model is represented by a class that inherits the django.db.models.Model.
# Each model has a number of class variables, each of which represents a database field in the model.

# Each filed is represented by an instance of a Field class.
# e.g. CharField for character fields and DateTimeField for date times.
# This tells Django what type of data each field holds.

# Some field classes have required arguments.
# e.g. CharField requires that you give it a max_length.


# [SECTION] Migration

# by running makemigrations, you're telling django that you've made some changes to your models.
# Syntax: python manage.py makemigrations <applicationName> this will stage only before migrating like git add .
# e.g. python manage.py makemigrations todolist

# Migrate after staging the models
# python manage.py sqlmigrate <applicationName> <id of makemigration>
# e.g. python manage.py sqlmigrate todolist 0001
# last process: python manage.py migrate

# to run python shell
# python manage.py shell

# from django.utils import timezone;
# todoitem = ToDoItem(task_name='Eat', description='dinner', date_created=timezone.now())
# todoitem.save()

# groceryitems = GroceryItem(item_name='Milk', description='2L', date_created=timezone.now())

# installation of mysql_connector and mysql
# pip install mysql_connector mysql

# create super user for windows
# winpty python manage.py createsuperuser

